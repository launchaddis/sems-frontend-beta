import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    children: [
       
      {
        path: 'employees',
        loadChildren: './employee/employee.module#EmployeeModule',
        data: {
          title: 'Employee Table'
        }
      }, 
      {
        path: 'attendances',
        loadChildren: './attendance/attendance.module#AttendanceModule',
        data: {
          title: 'Attendance Table'
        }
      }, 
      {
        path: 'payrolls',
        loadChildren: './payroll/payroll.module#PayrollModule',
        data: {
          title: 'Payroll Table'
        }
      }, 
      {
        path: 'departments',
        loadChildren: './department/department.module#DepartmentModule',
        data: {
          title: 'Department Table'
        }
      }  
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
