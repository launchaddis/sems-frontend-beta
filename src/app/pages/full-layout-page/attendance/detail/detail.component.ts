import { Component, ChangeDetectionStrategy, OnInit, EventEmitter } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { switchMap } from 'rxjs/operators/switchMap';
import { CalendarEvent } from 'angular-calendar';
import { Router, ActivatedRoute } from '@angular/router';
import { AttendancesService } from 'app/services/attendances/attendances.service';
import {
  addDays,
  subDays,
  isSameMonth,
  isSameDay,
  startOfMonth,
  endOfMonth,
  getMonth,
  getYear,
  startOfWeek,
  endOfWeek,
  startOfDay,
  endOfDay,
  format
} from 'date-fns';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import { colors } from '../demo-utils/colors';
import { Subscriber } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from '../../../../services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';

interface Attendance {
  year: number;
  month: number;
  day: number;
  shift: string;
  type: string;
  from: {
    hour: number,
    minute: number
  };
  to: {
    hour: number,
    minute: number
  };
}

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: 'detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [AttendancesService, ToastrService]
})
export class DetailAttendanceComponent implements OnInit {
  
  public _attendancesService;
  public id;
  public waiting = false;
  public error = false;
  public data: any = {
      employee : {
        picture:''
      }
    };

  public currentMonth = {
    year: getYear(new Date()),
    month: getMonth(new Date())+1
}  

  view: string = 'month';

  viewDate: Date = new Date();

  events$: Observable<Array<CalendarEvent<{ film: Attendance }>>>;

  resetMonth: EventEmitter<String> = new EventEmitter();
  previousMonth: EventEmitter<Date> = new EventEmitter();
  nextMonth: EventEmitter<Date> = new EventEmitter();

  activeDayIsOpen: boolean = false;

  constructor(private http: HttpClient,_attendancesService : AttendancesService,
              private router:Router, route: ActivatedRoute, private toastService: ToastrService,
              private authService:AuthenticationService) {
            this._attendancesService = _attendancesService;
            this.id = route.snapshot.params.id;
             }

  ngOnInit(): void {
    this.fetchEvents(); 
    
    this.previousMonth.subscribe((viewDate:Date) => {
   
      this.viewDate = viewDate;

      if(this.currentMonth.month == 1){
        this.currentMonth.year = this.currentMonth.year -1 ;
        this.currentMonth.month = 12       
      }
      else
      this.currentMonth.month = this.currentMonth.month -1
  
      this.fetchEvents();
      
    });

    this.nextMonth.subscribe((viewDate:Date) => {
   
      this.viewDate = viewDate;

      if(this.currentMonth.month == 12){
        this.currentMonth.year = this.currentMonth.year +1 ;
        this.currentMonth.month = 1       
      }
      else
      this.currentMonth.month = this.currentMonth.month +1
  
      this.fetchEvents();
    
    });
  }

  fetchEvents(): void {

    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }[this.view];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }[this.view];

    this.getAttendance(this.currentMonth,this.viewDate);
    
  }

  public reset(): void {

    this.currentMonth = {
      year: getYear(new Date()),
      month: getMonth(new Date())+1
    }
    this.viewDate = new Date();
    this.fetchEvents();
    
  }

  public getAttendance(month,viewDate){

    this._attendancesService.getMonthlyAttendance(this.id,month).subscribe(res => {
      this.data = res.data;      
    },
    error => {
      if(error.status == 403){
          this.authService.logout();
          this.router.navigateByUrl('/');
      }
    }
  );
    this.events$ = 
    this._attendancesService.getMonthlyAttendance(this.id,month)
      .pipe( 
      map(({ attendance }: { attendance: Attendance[] }) => {
       
          return attendance.map((film: Attendance) => {
    
            if(film.type == 'present'){
              return {
                title: film.type + " " + film.shift + " (" + film.from.hour + ":" + film.from.minute + " - "  + film.to.hour + ":" + film.to.minute + ")",
                start: subDays(addDays(startOfMonth(viewDate), film.day), 1),
                color: colors[film.type],
                meta: {
                  film
                }
              };
            }
            else{
              return {
                title: film.type + " " + film.shift,
                start: subDays(addDays(startOfMonth(viewDate), film.day), 1),
                color: colors[film.type],
                meta: {
                  film
                }
              };
            }
            
          });
        }),
        catchError((err, caught) => {         
          if( err.error.type == 'ATTENDANCE_COLLECTION_PAGINATED_EMPTY'){
            this.toastService.typeError("The attendance for this month is not yet populated"); 
            return new Promise((resolve: any): void => {
              this._attendancesService.setMonthlyAttendance(month).subscribe((data) => {
                this.toastService.typeSuccess("The attendance for this month is successfully populated"); 
              this.fetchEvents();
                resolve(data.data);
              },
                (error) =>  this.toastService.typeError("Unable to populate attendance")
              );
           });
            
          }
          else{
            this.toastService.typeError("Something happened, please check your connection");             
          }
          
          return Observable.empty();
        })
      );

  }

  dayClicked({
    date,
    events
  }: {
    date: Date;
    events: Array<CalendarEvent<{ film: Attendance }>>;
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  eventClicked(event: CalendarEvent<{ film: Attendance }>): void {
    // window.open(
    //   `https://beta.sems.addislaunch.com/attendance/${event.meta.film.day}`,
    //   '_blank'
    // );
  }

  
}