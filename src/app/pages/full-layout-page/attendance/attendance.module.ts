import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CalendarModule, CalendarDateFormatter } from 'angular-calendar';

import { AttendanceComponent } from './attendance.component';
import { DetailAttendanceComponent } from './detail/detail.component';
import { CreateAttendanceComponent } from './create/create.component';
import { DemoUtilsModule } from './demo-utils/module';
import { HttpClientModule } from '@angular/common/http';




const routes: Routes = [
    {
        "path": "",
        "children": [
            {
                "path": "",
                "component": AttendanceComponent
            },
            {
                "path": "create",
                "component": CreateAttendanceComponent
            },
            {
                "path": ":id/detail",
                "component": DetailAttendanceComponent
            }
        ]

    }
];
@NgModule({
    imports: [
        CommonModule, 
        CalendarModule.forRoot(),
        RouterModule.forChild(routes), 
        FormsModule, 
        NgxDatatableModule, 
        ReactiveFormsModule,
        HttpClientModule,
        DemoUtilsModule
    ], exports: [
        RouterModule
    ], declarations: [
        AttendanceComponent,
        CreateAttendanceComponent,
        DetailAttendanceComponent
    ]
})
export class AttendanceModule {



}