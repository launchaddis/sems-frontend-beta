import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { AttendancesService } from 'app/services/attendances/attendances.service';
import { ToastrService } from 'app/services/toastr/toastr.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./create.component.html",
    styleUrls: ['./create.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [AttendancesService, ToastrService]
})
export class CreateAttendanceComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _attendancesService;
    public csv:File;

    constructor(_attendancesService : AttendancesService,private router:Router, private toastService: ToastrService) {
            this._attendancesService = _attendancesService;
    }

    ngOnInit() {
    }    

    public onSubmit() {

        this.upload();
    
      }

    /* property of File type */
    fileChange($event){
        this.csv = $event.target.files[0];
    }      

    upload(): void {
        let _formData = new FormData();
        _formData.append("file", this.csv);
        let body = _formData;
    
        this._attendancesService.upload(body).subscribe(res => 
            {  
              console.log(res);
              this.submitted = true;
              this.toastService.typeSuccess();
              this.router.navigateByUrl('/attendances')
                               
            }, err => {
              this.toastService.typeError();              
            });
        }
}