export const colors: any = {
  abscent: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  weekend: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  present: {
    primary: '#33cc33',
    secondary: '#c2f0c2'
  }
};
