import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { AttendancesService } from 'app/services/attendances/attendances.service';
import { EmployeesService } from 'app/services/employees/employees.service';
import { Router } from '@angular/router';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from 'app/services/auth';
import {
    getMonth,
    getYear
  } from 'date-fns';

declare var require: any;
const data: any = require('../../../shared/data/company.json');

@Component({
    selector: 'app-attendance',
    templateUrl: './attendance.component.html',
    styleUrls: ['./attendance.component.scss'],
    providers: [AttendancesService,EmployeesService,ToastrService]
})

export class AttendanceComponent {
    
    private authService;
    private _attendancesService;
    private _employeesService;
    private toastService;
    public currentMonth = {
        year: getYear(new Date()),
        month: getMonth(new Date())+1
    }  
    
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
        { prop: 'attendance_id' },
        { prop: 'first_name' },
        { prop: 'last_name' },
        { name: 'email' },
        { name: 'gender' },
        { name: 'status' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(_employeesService: EmployeesService, _attendancesService: AttendancesService, private router: Router, 
        toastService: ToastrService, authService:AuthenticationService) {
        this._attendancesService = _attendancesService;
        this._employeesService = _employeesService;
        this.toastService = toastService;
        this.authService = authService;
        this.temp = [...data];
        this.rows = data;
        this.getAllAttendances();
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.first_name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getAllAttendances() {
        this._attendancesService.getAll().subscribe(data => {
            this.rows = data.json();
            const _ = data.json();

            if(this.rows.length == 0){
                this._attendancesService.setMonthlyAttendance(this.currentMonth).subscribe((data) => {
                    this.toastService.typeSuccess("The attendance for this month is successfully populated"); 
                  this.getAllAttendances(); 
            });
        }

            Object.keys(this.rows).forEach((key, val) => {
                if(_[key]._id.status == 0){
                    this.rows[key]._id.status = 'Inactive'; 
                }
                else if (_[key]._id.status == 1){
                    this.rows[key]._id.status = 'Active'; 
                }
            });

        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
          else{
            // this.getAllAttendances();
          }
        });
      }

    generateAttendance(){
        this._attendancesService.setMonthlyAttendance(this.currentMonth).subscribe(data => {
            this.toastService.typeSuccess("The attendance for this month is successfully generated");
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
          else{
            // this.getAllAttendances();
            this.toastService.typeError("Sorry, the attendance for this month cannot be generated");
          }
        });
    }  

    edit(id: any) {
        this.router.navigateByUrl('/attendances/' + id + '/edit');
    }

    detail(id: any) {
        this.router.navigateByUrl('/attendances/' + id + '/detail');
    }
  
    
}