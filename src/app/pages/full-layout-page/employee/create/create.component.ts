import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { EmployeesService } from 'app/services/employees/employees.service';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { DepartmentsService } from '../../../../services/departments/departments.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./create.component.html",
    encapsulation: ViewEncapsulation.None,
    providers: [EmployeesService, ToastrService, DepartmentsService]
})
export class CreateEmployeeComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _employeesService;
    public _departmentsService;
    public picture:File;
    public departments;
    public model = {
        ac_no: "",
        email: '',
        first_name: "",
        last_name: "",
        department: "",
        phone_number: null,
        salary: null,
        starting_date: "",
        birth_date: "",
        picture: "",
        status: 1,
        gender: ''
    };

    constructor(_employeesService : EmployeesService,_departmentsService: DepartmentsService,
        private router:Router, private toastService: ToastrService) {
            this._employeesService = _employeesService;
            this._departmentsService = _departmentsService;
    }

    public onSubmit() {

        this._employeesService.create(this.model).subscribe(res => 
          {  
            this.submitted = true;
            this.toastService.typeSuccess();
            this.router.navigateByUrl('/employees')
                             
          }, err => {
            this.toastService.typeError();              
          }
          );
    
      }

    ngOnInit() {
        this.getAllDepartments();
    }  
    
    getAllDepartments() {
        this._departmentsService.getAll().subscribe(data => {

                      this.departments = data.json().data;
                    },error => {

                    });
      }

}