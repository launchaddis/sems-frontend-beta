import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from 'app/services/employees/employees.service';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';
import { DepartmentsService } from '../../../../services/departments/departments.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./edit.component.html",
    styleUrls: ['./edit.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [EmployeesService,DepartmentsService,ToastrService]
})
export class EditEmployeeComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _employeesService;
    public _departmentsService;
    public departments;
    public id;
    public picture:File;
    public first = true;
    public photo;
    public url;
    public model = {
        ac_no: "",
        email: '',
        first_name: "",
        last_name: "",
        phone_number: null,
        department:"",
        salary: null,
        starting_date: "",
        birth_date: "",
        picture: "",
        status: 1,
        gender: ''
    };

    constructor(_employeesService : EmployeesService, _departmentsService: DepartmentsService
        ,private router:Router,
         route: ActivatedRoute,  private toastService: ToastrService,
         private authService:AuthenticationService) {
            this._employeesService = _employeesService;
            this._departmentsService = _departmentsService;
            this.id = route.snapshot.params.id;
    }

    public onSubmit() {

        if(this.picture) {
            this.upload();
            this.submit();
        } else {
            this.submit();
        }
      }

    public submit(){
        this._employeesService.update(this.id,this.model).subscribe(res => 
            {  
              this.submitted = true;
              this.toastService.typeSuccess();
              this.router.navigateByUrl('/employees');
                               
            }, err => {
              this.toastService.typeError();               
            }
            );        
    }  

    ngOnInit() {

        this.getAllDepartments();
        this._employeesService.get(this.id).subscribe(data => {

            const _ = data.json();         
            Object.keys(this.model).forEach((key, val) => {
                this.model[key] = _[key];
                if(key=="birth_date" || key=="starting_date"){
                    this.model[key] = this.formatDate(this.model[key]); 
                }else if(key=="department"){
                    this.model[key] = this.model[key]['_id']; 
                }
            });
            
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
        });

    }

    getAllDepartments() {
        this._departmentsService.getAll().subscribe(data => {

                      this.departments = data.json().data;
                    },error => {

                    });
      }

    /* property of File type */
    fileChange(
        event){
        this.picture = event.target.files[0];
        
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
        
            reader.onload = (event:any) => {
              this.url = event.target.result;
            }
        
            reader.readAsDataURL(event.target.files[0]);
          }
    }

    /* Now send your form using FormData */
    upload(): void {
    let _formData = new FormData();
    _formData.append("picture", this.picture);
    let body = _formData;

    this._employeesService.upload(this.id,body).subscribe(data => {

        // this.user = data.json();
    });
    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-');
    }

}