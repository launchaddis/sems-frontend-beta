import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { EmployeeComponent } from './employee.component';
import { CreateEmployeeComponent } from './create/create.component';
import { EditEmployeeComponent } from './edit/edit.component';
import { DetailEmployeeComponent } from './detail/detail.component';


const routes: Routes = [
    {
        "path": "",
        "children": [
            {
                "path": "",
                "component": EmployeeComponent
            },
            {
                "path": "create",
                "component": CreateEmployeeComponent
            },
            {
                "path": ":id/edit",
                "component": EditEmployeeComponent
            },
            {
                "path": ":id/detail",
                "component": DetailEmployeeComponent
            }
        ]

    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), FormsModule, NgxDatatableModule, ReactiveFormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        EmployeeComponent,
        CreateEmployeeComponent,
        EditEmployeeComponent,
        DetailEmployeeComponent
    ]
})
export class EmployeeModule {



}