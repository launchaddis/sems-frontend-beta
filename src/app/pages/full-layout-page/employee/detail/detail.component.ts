import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesService } from 'app/services/employees/employees.service';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./detail.component.html",
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [EmployeesService,ToastrService]
})
export class DetailEmployeeComponent implements OnInit {

    public _employeesService;
    public id;
    public photo;
    public data = {
        ac_no: "",
        email: '',
        first_name: "",
        last_name: "",
        phone_number: null,
        salary: null,
        starting_date: "",
        birth_date: "",
        picture: "",
        status: 1,
        gender: ''
    };

    constructor(_employeesService : EmployeesService,private router:Router,
         route: ActivatedRoute,  private toastService: ToastrService,
         private authService:AuthenticationService) {
            this._employeesService = _employeesService;
            this.id = route.snapshot.params.id;
    }

    ngOnInit() {

        this._employeesService.get(this.id).subscribe(data => {

            const _ = data.json();
            Object.keys(this.data).forEach((key, val) => {
                this.data[key] = _[key];
                if(key=="birth_date" || key=="starting_date"){

                    this.data[key] = this.formatDate(this.data[key]); 
                }
                if(key=="gender"){
                     
                    if(this.data[key] == '0'){
                        this.data[key] = 'Male'; 
                    }
                    else if(this.data[key] == '1'){
                        this.data[key] = 'Female'; 
                    }
          
                }
            });
            
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
        });       

    }

    formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;
    
        return [year, month, day].join('-');
    }
}