import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { EmployeesService } from 'app/services/employees/employees.service';
import { Router } from '@angular/router';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from 'app/services/auth';

declare var require: any;
const data: any = require('../../../shared/data/company.json');

@Component({
    selector: 'app-employee',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.scss'],
    providers: [EmployeesService,ToastrService]
})

export class EmployeeComponent {
    
    private authService;
    private _employeesService;
    private toastService;
    
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
        { prop: 'employee_id' },
        { prop: 'first_name' },
        { prop: 'last_name' },
        { name: 'email' },
        { name: 'gender' },
        { name: 'status' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(_employeesService: EmployeesService, private router: Router, 
        toastService: ToastrService, authService:AuthenticationService) {
        this._employeesService = _employeesService;
        this.toastService = toastService;
        this.authService = authService;
        this.temp = [...data];
        this.rows = data;
        this.getAllEmployees();
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.first_name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getAllEmployees() {
        this._employeesService.getAll().subscribe(data => {
            this.rows = data.json().data;
            const _ = data.json().data;
            
            Object.keys(this.rows).forEach((key, val) => {
                if(_[key].gender == 0){
                    this.rows[key].gender = 'Male'; 
                }
                else if (_[key].gender == 1){
                    this.rows[key].gender = 'Female'; 
                }
                if(_[key].status == 0){
                    this.rows[key].status = 'Inactive'; 
                }
                else if (_[key].status == 1){
                    this.rows[key].status = 'Active'; 
                }
            });
            this.temp = data.json().data;
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
        });
      }

    edit(id: any) {
        this.router.navigateByUrl('/employees/' + id + '/edit');
    }

    detail(id: any) {
        this.router.navigateByUrl('/employees/' + id + '/detail');
    }
  
    
}