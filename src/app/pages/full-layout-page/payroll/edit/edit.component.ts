import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { PayrollsService } from 'app/services/payrolls/payrolls.service';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';
import {
    getMonth,
    getYear
  } from 'date-fns';
declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./edit.component.html",
    styleUrls: ['./edit.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [PayrollsService,ToastrService]
})
export class EditPayrollComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _payrollsService;
    public id;
    public month_input;
    public model = {
        date_created : "", 
        days_worked : "",
        employee : {
            department : "",
            email: "",
            phone_number:"",
            first_name : "",
            salary : ""
        },
        gross_salary : 0,
        gross_salary_taxbel : 0,
        income_tax : 0,
        over_time : 0,
        employee_contribution : 0,
        loan : 0,
        allowance : 0,
        last_modified : "",
        month : "",
        net_pay : 0,
        penality : 0,
        total_deduction : 0,
        work_days : 0,
        year : ""
    };
    public currentMonth = {
        year: getYear(new Date()),
        month: getMonth(new Date())+1
    }

    constructor(_payrollsService : PayrollsService,private router:Router,
         route: ActivatedRoute,  private toastService: ToastrService,
        private authService:AuthenticationService) {
            this._payrollsService = _payrollsService;
            this.id = route.snapshot.params.id;
    }

    public onSubmit() {

        this._payrollsService.update(this.id,this.model).subscribe(res => 
            {  
              this.submitted = true;
              this.toastService.typeSuccess();
              this.router.navigateByUrl('/payrolls');
                               
            }, err => {
              this.toastService.typeError();               
            }
            );  
      }

    ngOnInit() {          
        this.getPayroll();
    }

    getPayroll(){
        this._payrollsService.getMonthlyPayroll(this.id,this.currentMonth).subscribe(data => {
            
            const isEmpty = this.isEmpty(data.json());

            if(isEmpty){ 
            this.toastService.typeError("The payroll for "+ this.currentMonth.month + "/" +
            this.currentMonth.year+" is empty"); 
                
            this._payrollsService.setMonthlyPayroll(this.currentMonth).subscribe(data => {

                if(data.json().err_status == "NO_ATTENDANCE_GENERATED"){
                    this.toastService.typeError("You need to generate the attendance for this month first");
                    
                }
                else if (data.json().err_status == "ATTENDANCE_GENERATED"){
                    this.toastService.typeSuccess("The payroll for "+ this.currentMonth.month + "/" +
                    this.currentMonth.year+" is successfuly generated");
                    this.getPayroll();      
                }
                }
            ); 
            }
            else{                   
            
            const _ = data.json();
            let tempMonth,tempYear; 

            Object.keys(this.model).forEach((key, val) => {
                this.model[key] = _[key];
                if(key=="month"){
                    tempMonth = ('0' + this.model[key]).slice(-2)
                }
                if(key=="year"){
                    tempYear =    this.model[key]; 
                }
            });

            this.month_input = tempYear+"-"+tempMonth;      
            }
                
            },
            error => {
              if(error.status == 403){
                  this.authService.logout();
                  this.router.navigateByUrl('/');
              }
            });   
    }

    isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
    
        return JSON.stringify(obj) === JSON.stringify({});
    }

    updateMonth(data){        
        const month = new Date(data);  
        this.currentMonth.month = month.getMonth() + 1;
        this.currentMonth.year = month.getFullYear();
        this.getPayroll()    
    }


    parse(val){
        if(val === '' || val === undefined || !val ){
            return 0;
        }
        else
          return parseInt(val);
    }

    getTax(){
        const basic_sallary = this.parse(this.model.gross_salary_taxbel);
        let deduction = [0,60,142.5,302.5,565,955,1500]
        let amount     = [600,1650,3200,5250,7800,10900]
    
        if(basic_sallary <= amount[0]){
          return ((basic_sallary * .00) - deduction[0]); 
        }else if(basic_sallary > amount[0] && basic_sallary <= amount[1]){
            return ((basic_sallary * .10) - deduction[1]);
    
        }else if(basic_sallary > amount[1] && basic_sallary <= amount[2]){
            return ((basic_sallary * .15) - deduction[2]);
        }else if(basic_sallary > amount[2] && basic_sallary <= amount[3]){
            return ((basic_sallary * .20) - deduction[3]); 
    
        }else if(basic_sallary > amount[3] && basic_sallary <= amount[4]){
            return ((basic_sallary * .25) - deduction[4]); 
    
        }else if(basic_sallary > amount[4] && basic_sallary <= amount[5]){
            return ((basic_sallary * .30) - deduction[5]); 
    
        }else if(basic_sallary >= amount[5]){
            return ((basic_sallary * .35) - deduction[6]); 
        }
    
      }

      updateGross(event: any) { // without type info

        this.model.gross_salary = this.parse(this.model.employee.salary) + this.parse(this.model.over_time) 
        + this.parse(this.model.allowance) ;        
        
      }

      updateDeduction(event: any) { // without type info

        const tempSalary = (this.parse(this.model.days_worked) * this.parse(this.model.employee.salary))/this.parse(this.model.work_days);
        
         this.model.penality = this.parse(this.model.employee.salary) - tempSalary;  
         this.model.total_deduction =   this.parse(this.model.penality) + this.parse(this.model.loan) + 
                                        this.parse(this.model.employee_contribution) + this.parse(this.model.income_tax);  
        this.model.net_pay = this.model.gross_salary - this.model.total_deduction;      
        
      }

}