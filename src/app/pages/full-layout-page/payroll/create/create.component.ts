import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { PayrollsService } from 'app/services/payrolls/payrolls.service';
import { ToastrService } from 'app/services/toastr/toastr.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./create.component.html",
    encapsulation: ViewEncapsulation.None,
    providers: [PayrollsService, ToastrService]
})
export class CreatePayrollComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _payrollsService;
    public picture:File;
    public model = {
        payroll_id: "",
        email: '',
        first_name: "",
        last_name: "",
        phone_number: null,
        salary: null,
        starting_date: "",
        birth_date: "",
        picture: "",
        status: 1,
        gender: ''
    };

    constructor(_payrollsService : PayrollsService,private router:Router, private toastService: ToastrService) {
            this._payrollsService = _payrollsService;
    }

    public onSubmit() {

        this._payrollsService.create(this.model).subscribe(res => 
          {  
            this.submitted = true;
            this.toastService.typeSuccess();
            this.router.navigateByUrl('/payrolls')
                             
          }, err => {
            this.toastService.typeError();              
          }
          );
    
      }

    ngOnInit() {
    }    

}