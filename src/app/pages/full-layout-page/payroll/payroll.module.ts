import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PayrollComponent } from './payroll.component';
import { CreatePayrollComponent } from './create/create.component';
import { EditPayrollComponent } from './edit/edit.component';
import { DetailPayrollComponent } from './detail/detail.component';

const routes: Routes = [
    {
        "path": "",
        "children": [
            {
                "path": "",
                "component": PayrollComponent
            },
            {
                "path": "create",
                "component": CreatePayrollComponent
            },
            {
                "path": ":id/edit",
                "component": EditPayrollComponent
            },
            {
                "path": ":id/detail",
                "component": DetailPayrollComponent
            }
        ]

    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), 
        FormsModule, NgxDatatableModule, ReactiveFormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        PayrollComponent,
        CreatePayrollComponent,
        EditPayrollComponent,
        DetailPayrollComponent
    ]
})
export class PayrollModule {



}