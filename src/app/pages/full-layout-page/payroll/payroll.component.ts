import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";
import { PayrollsService } from 'app/services/payrolls/payrolls.service';
import { Router } from '@angular/router';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from 'app/services/auth';

declare var require: any;
const data: any = require('../../../shared/data/company.json');

@Component({
    selector: 'app-payroll',
    templateUrl: './payroll.component.html',
    styleUrls: ['./payroll.component.scss'],
    providers: [PayrollsService,ToastrService]
})

export class PayrollComponent {
    
    private authService;
    private _payrollsService;
    private toastService;
    
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
        { prop: 'payroll_id' },
        { prop: 'first_name' },
        { prop: 'last_name' },
        { name: 'email' },
        { name: 'gender' },
        { name: 'status' },
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor(_payrollsService: PayrollsService, private router: Router, 
        toastService: ToastrService, authService:AuthenticationService) {
        this._payrollsService = _payrollsService;
        this.toastService = toastService;
        this.authService = authService;
        this.temp = [...data];
        this.rows = data;
        this.getAllPayrolls();
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.first_name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }

    getAllPayrolls() {
        this._payrollsService.getAll().subscribe(data => {
            this.rows = data.json().data;
            const _ = data.json().data;
            
            Object.keys(this.rows).forEach((key, val) => {
                if(_[key].gender == 0){
                    this.rows[key].gender = 'Male'; 
                }
                else{
                    this.rows[key].gender = 'Female'; 
                }
            });
            this.temp = data.json().data;
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
          else{
            // this.getAllPayrolls();
          }
        });
      }

    edit(id: any) {
        this.router.navigateByUrl('/payrolls/' + id + '/edit');
    }

    detail(id: any) {
        this.router.navigateByUrl('/payrolls/' + id + '/detail');
    }
  
    
}