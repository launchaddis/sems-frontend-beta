import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PayrollsService } from 'app/services/payrolls/payrolls.service';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';
import {
    getMonth,
    getYear
  } from 'date-fns';
declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./detail.component.html",
    styleUrls: ['./detail.component.css'],
    encapsulation: ViewEncapsulation.None,
    providers: [PayrollsService,ToastrService]
})
export class DetailPayrollComponent implements OnInit {

    public _payrollsService;
    public id;
    public photo;
    public month_input;
    public data = {
        date_created : "", 
        days_worked : "",
        employee : {
            department : "",
            email: "",
            phone_number:"",
            first_name : "",
            salary : "",
            _id : "",
            employee_id:""
        },
        gross_salary : "",
        gross_salary_taxbel : "",
        income_tax : "",
        over_time : "",
        employee_contribution : "",
        loan : "",
        allowance : "",
        last_modified : "",
        month : "",
        net_pay : "",
        penality : "",
        total_deduction : "",
        work_days : "",
        year : ""
    };
    public currentMonth = {
        year: getYear(new Date()),
        month: getMonth(new Date())+1
    } 

    public model: any = { date: { year: 2018, month: 10, day: 9 } };

    constructor(_payrollsService : PayrollsService,private router:Router,
         route: ActivatedRoute,  private toastService: ToastrService,
         private authService:AuthenticationService) {
            this._payrollsService = _payrollsService;
            this.id = route.snapshot.params.id;
    }

    ngOnInit() {          
        this.getPayroll();
    }

    getPayroll(){
        this._payrollsService.getMonthlyPayroll(this.id,this.currentMonth).subscribe(data => {
            
            const isEmpty = this.isEmpty(data.json());

            if(isEmpty){ 
            this.toastService.typeError("The payroll for "+ this.currentMonth.month + "/" +
            this.currentMonth.year+" is empty"); 
                
            this._payrollsService.setMonthlyPayroll(this.currentMonth).subscribe(data => {

                if(data.json().err_status == "NO_ATTENDANCE_GENERATED"){
                    this.toastService.typeError("You need to generate the attendance for this month first");
                    
                }
                else if (data.json().err_status == "ATTENDANCE_GENERATED"){
                    this.toastService.typeSuccess("The payroll for "+ this.currentMonth.month + "/" +
                    this.currentMonth.year+" is successfuly generated");
                    this.getPayroll();      
                }
                }
            ); 
            }
            else{                   
            this.data = data.json();
            const _ = data.json();
            let tempMonth,tempYear; 

            Object.keys(this.data).forEach((key, val) => {
                this.data[key] = _[key];
                if(key=="month"){
                    tempMonth = ('0' + this.data[key]).slice(-2)
                }
                if(key=="year"){
                    tempYear =    this.data[key]; 
                }
  
            });
            this.month_input = tempYear+"-"+tempMonth;      
            }
                
            },
            error => {
              if(error.status == 403){
                  this.authService.logout();
                  this.router.navigateByUrl('/');
              }
            });   
    }

    isEmpty(obj) {
        for(var prop in obj) {
            if(obj.hasOwnProperty(prop))
                return false;
        }
    
        return JSON.stringify(obj) === JSON.stringify({});
    }

    updateMonth(data){        
        const month = new Date(data);  
        this.currentMonth.month = month.getMonth() + 1;
        this.currentMonth.year = month.getFullYear();
        this.getPayroll()    
    }

    print(){
    window.print();
    }

}