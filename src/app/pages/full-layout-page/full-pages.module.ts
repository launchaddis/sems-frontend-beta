import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';

import { FullPagesRoutingModule } from "./full-pages-routing.module";

@NgModule({
    imports: [
        CommonModule,
        FullPagesRoutingModule,
        FormsModule
    ],
    declarations: [       
    
    ]
})
export class FullPagesModule { }
