import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router } from '@angular/router';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { DepartmentsService } from '../../../../services/departments/departments.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./create.component.html",
    encapsulation: ViewEncapsulation.None,
    providers: [DepartmentsService, ToastrService, DepartmentsService]
})
export class CreateDepartmentComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _departmentsService;
    public model = {
        name: "",
        shift_starting_time: "",
        shift_ending_time: "",
    };

    constructor(_departmentsService: DepartmentsService,
        private router:Router, private toastService: ToastrService) {
            this._departmentsService = _departmentsService;
            this._departmentsService = _departmentsService;
    }

    public onSubmit() {

        this._departmentsService.create(this.model).subscribe(res => 
          {  
            this.submitted = true;
            this.toastService.typeSuccess();
            this.router.navigateByUrl('/departments')
                             
          }, err => {
            this.toastService.typeError();              
          }
          );
    
      }

    ngOnInit() {
    }  
    

}