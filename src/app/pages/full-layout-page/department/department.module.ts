import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { DepartmentComponent } from './department.component';
import { CreateDepartmentComponent } from './create/create.component';
import { EditDepartmentComponent } from './edit/edit.component';


const routes: Routes = [
    {
        "path": "",
        "children": [
            {
                "path": "",
                "component": DepartmentComponent
            },
            {
                "path": "create",
                "component": CreateDepartmentComponent
            },
            {
                "path": ":id/edit",
                "component": EditDepartmentComponent
            }
        ]

    }
];
@NgModule({
    imports: [
        CommonModule, RouterModule.forChild(routes), FormsModule, NgxDatatableModule, ReactiveFormsModule
    ], exports: [
        RouterModule
    ], declarations: [
        DepartmentComponent,
        CreateDepartmentComponent,
        EditDepartmentComponent
    ]
})
export class DepartmentModule {



}