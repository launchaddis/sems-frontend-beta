import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'app/services/toastr/toastr.service';
import { AuthenticationService } from '../../../../services/auth/authentication.service';
import { DepartmentsService } from '../../../../services/departments/departments.service';

declare let mApp: any;

@Component({
    selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
    templateUrl: "./edit.component.html",
    styleUrls: ['./edit.component.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: [DepartmentsService,DepartmentsService,ToastrService]
})
export class EditDepartmentComponent implements OnInit {

    @ViewChild('f') floatingLabelForm: NgForm;

    public submitted = false;
    public _departmentsService;
    public id;
    public model = {
        name: "",
        shift_starting_time: "",
        shift_ending_time: "",
    };

    constructor(_departmentsService: DepartmentsService
        ,private router:Router,
         route: ActivatedRoute,  private toastService: ToastrService,
         private authService:AuthenticationService) {
            this._departmentsService = _departmentsService;
            this._departmentsService = _departmentsService;
            this.id = route.snapshot.params.id;
    }

    public onSubmit(){
        this._departmentsService.update(this.id,this.model).subscribe(res => 
            {  
              this.submitted = true;
              this.toastService.typeSuccess();
              this.router.navigateByUrl('/departments');
                               
            }, err => {
              this.toastService.typeError();               
            }
            );        
    }  

    ngOnInit() {

        this._departmentsService.get(this.id).subscribe(data => {

            const _ = data.json();
            console.log(_);
                     
            Object.keys(this.model).forEach((key, val) => {
                this.model[key] = _[key];
            });
            
        },
        error => {
          if(error.status == 403){
              this.authService.logout();
              this.router.navigateByUrl('/');
          }
        });

    }

}