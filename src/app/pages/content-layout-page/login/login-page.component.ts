import { Component, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from 'app/services/auth/authentication.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss']
})

export class LoginPageComponent {

    model: any = {};
    res;
    error = null;
    loading = false;
    
    @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private _authService: AuthenticationService,
        private route: ActivatedRoute) { }

    // On submit button click    
    onSubmit() {

        this.loading = true;
        this._authService.login(this.model)
        .subscribe(
        data => {
            this.loading = false;
            this._authService.logout();
            this.res = data.json();
            this._authService.saveAccessData(this.res.token);
            this._authService.saveProfile(this.res.user);            
            this.router.navigate(['/employees']);
        },
        error => {
            this.loading = false;
            this.error = error.json().message;
            if(error.json().status== 400){
                this.error = error.json().message;;    
              }
              else {
                this.error = 'Unable to connect please try again';
              }
        });
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}