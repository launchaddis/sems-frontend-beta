import { Component } from '@angular/core';
import { AuthenticationService } from 'app/services/auth';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})

export class NavbarComponent{
    constructor(private _router: Router,
        private _authService: AuthenticationService){
        
    }

    logout(): void {
        // reset login status
        this._authService.logout();
        this._router.navigate(['/login']);
    }
}
