import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './../auth/authentication.service';


@Injectable()
export class DepartmentsService extends BaseService implements HttpServiceInterface {
  constructor(http: Http,auth:AuthenticationService) {
    super(http, 'departments',auth);
}
}
