import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response,  Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './../auth/authentication.service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class AttendancesService extends BaseService implements HttpServiceInterface {
  constructor(http: Http,auth:AuthenticationService,private httpClient: HttpClient) {
    super(http, 'attendance',auth);
}

public getAll() {
  return this.http.get(`http://api.sems.addislaunch.com/empATT`,this.options);
}

public getMonthlyAttendance(id,data) {
  const headers = new HttpHeaders()
  .set('Authorization', 'Bearer '+ this.authService.getAccessToken())
  .set('Content-Type', 'application/json');
  return this.httpClient.post(`${this.base_url}/${id}`, data, {headers});
}

public setMonthlyAttendance(data) {
  return this.http.post(`http://api.sems.addislaunch.com/empAtt`, data,this.options);
}

public upload(data) {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
  
  this.options = new RequestOptions({headers: headers});
  return this.http.post(this.base_url + `/upload/CSV`, data, this.options);
}
}
