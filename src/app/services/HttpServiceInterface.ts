export interface HttpServiceInterface {
  getAll();

  get(id: string);

  create(data: any);

  update(id: string, data: any);

  delete(id: string);

}
