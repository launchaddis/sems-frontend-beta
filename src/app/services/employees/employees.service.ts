import { HttpServiceInterface } from '../HttpServiceInterface';
import { BaseService } from '../BaseService';
import { Http, Response,  Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './../auth/authentication.service';


@Injectable()
export class EmployeesService extends BaseService implements HttpServiceInterface {
  constructor(http: Http,auth:AuthenticationService) {
    super(http, 'employees',auth);
}

public upload(id, data) {
  const headers = new Headers();
  headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
  
  this.options = new RequestOptions({headers: headers});
  return this.http.put(this.base_url + `/${id}/pic`, data, this.options);
}
}
