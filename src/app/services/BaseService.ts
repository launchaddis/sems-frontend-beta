import { HttpServiceInterface } from './HttpServiceInterface';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { OnInit, Injectable } from '@angular/core';
import { AuthenticationService } from './auth/authentication.service';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class BaseService implements HttpServiceInterface, OnInit {

  private model: string;
  public headers;
  public options;

  public base_url = 'http://api.sems.addislaunch.com/';

  constructor(public http: Http, modelX: string,public authService:AuthenticationService) {
    this.model = modelX;
    this.base_url = this.base_url + this.model;
    // this.authService;

    const headers = new Headers();
    headers.append('Authorization', 'Bearer '+ this.authService.getAccessToken());
    headers.append('Content-Type', 'application/json');

    this.options = new RequestOptions({headers: headers});
  }

  ngOnInit(): void {
    
  }

  public getAll() {

    return this.http.get(`${this.base_url}?per_page=1000`,this.options);
  }

  public get(id) {
    return this.http.get(`${this.base_url}/${id}`,this.options);
  }

  public create(data) {
    return this.http.post(`${this.base_url}/`, data,this.options);
  }

  public update(id, data) {
    return this.http.put(`${this.base_url}/${id}`, data, this.options);
  }

  public delete(id) {
    return this.http.delete(`${this.base_url}/${id}`, this.options);
  }

}
